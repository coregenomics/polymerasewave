## install.packages("BiocManager")
## Supply the names of packages below to BiocManager::install(...)
suppressPackageStartupMessages({
    library(dplyr)                      # %>%, mutate, arrange
    library(GenomicAlignments)          # Count reads using readGAlignments()
    library(org.Hs.eg.db)               # Gene annotations including symbols
    library(TxDb.Hsapiens.UCSC.hg38.knownGene) # hg38 annotations.
    library(stringr)                    #str_*
    library(rtracklayer)                # Read in BED files
    library(BiocParallel)
})

## We want alignments for the GPC5 gene.
gene_name <- "GPC5"
gene_id <- select(org.Hs.eg.db, keys = gene_name, keytype = "SYMBOL",
                  columns = "ENTREZID")$ENTREZID
stopifnot(length(gene_id) == 1)
gene <- genes(TxDb.Hsapiens.UCSC.hg38.knownGene,
              filter = list(gene_id = gene_id))

## Generate a list of our BAM files and extract the collection time
## and replicate ID.
dir_align <- "/share/Luke/alignments/bam"
if (! dir.exists(dir_align)) {
    stop("Could not find aligned reads directory ", dir_align,
         " on this computer.")
}
tbl_bams <- tibble::tibble(
                        path = Sys.glob(
                            file.path(dir_align,
                                      "*rep*DmHg_sorted*.bam"))) %>%
    mutate(
        ## Ignore "NAs introduced by coercion" for asynchronous (AS) timepoint.
        times = suppressWarnings(
            basename(path) %>%
            str_remove_all("(^M|[a-z][-]rep.*)") %>%
            str_replace_all("-", ".") %>%
            str_replace_all("AS", "NA") %>%
            as.numeric()),
        replicate = basename(path) %>%
            str_match("(.)[-]rep") %>%
            .[, 2],
        mitotic = basename(path) %>%
            str_detect("^M")) %>%
    arrange(mitotic, times)

## Import reads associated with the gene.
##
## Subset seqlevels as workaround for "seqlevels(param) not in BAM header".
seqlevels(gene) <- seqlevelsInUse(gene)
bams <- BamViews(pull(tbl_bams, path), bamRanges = gene)
gal <- readGAlignments(bams)

## Merge replicates.
##
## Names do not allow NA, so convert to "AS"; they also cannot start with
## numbers, so prefix with "M".
names(gal) <- tbl_bams %>%
    mutate(times = ifelse(! mitotic, "AS", str_c("M", times))) %>%
    pull(times)
gr <- gal %>% unlist() %>% granges()
reads <- splitAsList(gr, names(gr))

## Count reads.
counts <- endoapply(reads, function(x)
    coverage(x)[seqnames(gene)] %>%
    as("GRanges") %>%
    `strand<-`(value = strand(gene))) %>%
    restrict(start = start(gene), end = end(gene))
counts_rle <- lapply(reads, function(x)
    coverage(x)[seqnames(gene)])
## Rle seems like the more appropriate format.

## 50 bp windows.
size <- 50
windows <- unlist(slidingWindows(setNames(gene, NULL), size, size),
                  use.names = FALSE)
counts_50 <- lapply(reads, countOverlaps, query = windows)

## Save data files.
usethis::use_data(gene, reads, counts, counts_rle, counts_50,
                  overwrite = TRUE)

## Reads for all long genes.
genes_all <- genes(TxDb.Hsapiens.UCSC.hg38.knownGene) # nolint

## dREG stringent tends to capture TSS sites only.
dreg <- import("/share/Luke/dreg/TREs/LW_M_all.fastq_dREG.out.bedgraph.gz.bed_dREG_HD_stringent.bed") # nolint

## Find active genes by overlapping gene start single base with dREG
## stringent regions.
promoters_all <- promoters(genes_all, upstream = 0, downstream = 1)
promoters_active <- promoters_all[promoters_all %over% dreg]
genes_active <- genes_all[promoters_all %over% dreg]

## Subset to genes at least 360 kb long.
width_min <- 360e3
genes <- genes_active[width(genes_active) > width_min]
promoters <- promoters_active[width(genes_active) > width_min]

## Can't remove genes with neighbors downstream... need to consider them all
## and deal with the artifacts they introduce.
follow(genes, genes_active) %>% summary
follow(promoters, promoters_active) %>% summary

## Import reads associated with genes.
##
## Subset seqlevels as workaround for "seqlevels(param) not in BAM header".
seqlevels(genes) <- seqlevelsInUse(genes)
bams <- BamViews(pull(tbl_bams, path), bamRanges = genes)
gal <- readGAlignments(bams)

## Merge replicates.
##
## Names do not allow NA, so convert to "AS"; they also cannot start with
## numbers, so prefix with "M".
names(gal) <- tbl_bams %>%
    mutate(times = ifelse(! mitotic, "AS", str_c("M", times))) %>%
    pull(times)
gr <- gal %>% unlist() %>% granges()
reads <- splitAsList(gr, names(gr))
elementNROWS(reads)

## 50 bp windows.
size <- 50
windows <- slidingWindows(genes, size, size)
counts_all <-
    bplapply(windows, function(x) lapply(reads, countOverlaps, query = x))

gene_names <-
    select(org.Hs.eg.db,      # nolint
           keys = mcols(genes)$gene_id,
           keytype = "ENTREZID",
           columns = "SYMBOL")$SYMBOL

names(counts_all) <- gene_names
names(genes) <- gene_names

## Gather dreg data.
dreg_relaxed <- import("/share/Luke/dreg/TREs/LW_M_all.fastq_dREG.out.bedgraph.gz.bed_dREG_HD_relaxed.bed") # nolint
dreg_all <- GRangesList(stringent = dreg,
                        relaxed = dreg_relaxed)
is_dreg <-
    bplapply(windows, function(x) lapply(dreg_all, overlapsAny, query = x))
names(is_dreg) <- gene_names

lapply(is_dreg, function(x) lapply(x, sum))

## Import unmappable regions.
mappable_all <- import("/home/corelab/Users/GROseq_Utils/genomes/hg38/hg38_36mers_mappability.bedgraph.gz") # nolint

is_unmappable <-
    bplapply(windows, function(x) ! overlapsAny(x, mappable_all))
names(is_unmappable) <- gene_names

lapply(is_unmappable, sum)

## Save data files.
usethis::use_data(genes, counts_all, is_dreg, is_unmappable,
                  overwrite = TRUE)
