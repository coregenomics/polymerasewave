;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode
  (c-basic-offset . 4)
  (fill-column . 80)
  (flycheck-gcc-include-path
   "/home/omsai/R/x86_64-pc-linux-gnu-library/3.5/Rcpp/include/"
   "/home/omsai/R/x86_64-pc-linux-gnu-library/3.5/testthat/include/"
   "/home/omsai/R/x86_64-pc-linux-gnu-library/3.5/plogr/include/"
   "/usr/lib64/R/include/"
   "/usr/share/R/include/"))
 (ess-mode
  (fill-column . 79))
 (python-mode
  (fill-column . 79)))
