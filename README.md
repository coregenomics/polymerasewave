---
pagetitle: Overview
---

[![Pipeline status](https://gitlab.com/coregenomics/polymerasewave/badges/master/pipeline.svg)](https://gitlab.com/coregenomics/polymerasewave/pipelines)
[![R coverage](https://gitlab.com/coregenomics/polymerasewave/badges/master/coverage.svg?job=coverage)](https://coregenomics.gitlab.io/polymerasewave/coverage/index.html)

# Overview {-}
<img src="man/figures/polywave_logo_130x150.png" align="right" />

Measure genomic intervals travelled by polymerase in nascent RNA pause-release
experiment across timepoints using Markov chain Monte Carlo (MCMC) Bayesian
statistics.

## Installation {-}

You can install the released version of polymerasewave
from [GitLab](https://gitlab.com/coregenomics/polymerasewave) with:

``` r
# install.packages(c("BiocManager", "remotes"))
remotes::install_git("https://gitlab.com/coregenomics/polymerasewave.git",
                     repos = BiocManager::repositories())
```
