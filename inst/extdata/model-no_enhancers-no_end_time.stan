data {
  /* Data dimensions */
  int<lower=1> N;               /* Windows */
  int<lower=2> T;               /* Time points */

  /* Data */
  int<lower=0> x[T,N];       /* Counts for each time point*/
  real<upper=1> log_tx[T,N]; /* Pre-computed transcribed log-likelihood */
  real<upper=1> log_untx[T,N]; /* Pre-computed untranscribed log-likelihood */
  positive_ordered[T] mins;
  real<lower=0> polspeed_wpm;
}

transformed data {
  /* Choose values at the 95% quantile. */
  real rate_min_start = exponential_cdf(0.95, 1 / max(mins));
  real rate_alpha     = exponential_cdf(0.95, 1 / 50.0);
}

parameters {
  real<lower=0,upper=max(mins)> min_start;
  real<lower=0> alpha;          /* Hyperparameter */
  simplex[T] speeds_dirichlet;
}

model {
  min_start ~ exponential(rate_min_start);
  alpha ~ exponential(rate_alpha);
  speeds_dirichlet ~ dirichlet(alpha * rep_vector(1.0, T));

  for (t in 1:T)
    for (n in 1:N) {
      /* Not using vectorization by converting boundary to an integer,
         because that breaks sampling continuouity and efficiency. */
      if (n <
          polspeed_wpm * (speeds_dirichlet[t] * T) *
          (mins[t] - min_start))
        target += log_tx[t,n];
      else
        target += log_untx[t,n];
    }
}
